﻿using UnityEngine;

public class Door : MonoBehaviour
{
	public int nextRoomIndex = 0;
	public float openTime = 1.5f;

	private float currentTime = 0f;
	private bool opening = false;

	private Vector3 originalPos;
	private Vector3 finalPos;

	private AudioSource audioSource;

	void Start()
	{
		originalPos = transform.localPosition;
		finalPos = originalPos - new Vector3(0f, 13.95f, 0f);

		audioSource = GetComponent<AudioSource>();
		audioSource.clip = SoundManager.Instance.GetAudioClip(AudioNames.DOOR_OPEN);
	}

	public void Open()
	{
		opening = true;
		currentTime = 0f;
		audioSource.Play();
		LevelManager.Instance.EnablePlayersForRoom(nextRoomIndex);
	}

	public void Reset()
	{
		transform.localPosition = originalPos;
		opening = false;
	}

	void Update()
	{
		if (!opening)
			return;
		
		currentTime += Time.deltaTime;

		transform.localPosition = Vector3.Lerp(originalPos, finalPos, currentTime / openTime);
		opening = currentTime < openTime;
	}
}
