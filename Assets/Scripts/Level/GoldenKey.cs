﻿using UnityEngine;

public class GoldenKey : MonoBehaviour
{
	public Transform animatorTransform;
	
	private Transform playerTransform;

	void Start()
	{
		playerTransform = LevelManager.Instance.GetPlayerTransform();
	}
	
	void Update()
	{
		animatorTransform.transform.LookAt(playerTransform);
	}

	private void OnTriggerEnter(Collider collider)
    {
		if (collider.gameObject.layer == LevelManager.playerLayer)
		{
			gameObject.SetActive(true);
			GameManager.Instance.WinGame();
		}
	}
}