﻿using UnityEngine;

public class Level : MonoBehaviour
{
	public Transform playerSpawnPoint;
	public Transform enemySpawnPointsParent;

	public GameObject goldenKey;

	public Door[] doors;

	private int killsAmountToClearRoom = 0;
	private int currentRoom = 0;

	public void SetKillsAmountAndRoom(int amount, int room)
	{
		killsAmountToClearRoom = amount;
		currentRoom = room;
	}

	public void OnEnemyKill()
	{
		killsAmountToClearRoom--;

		if (killsAmountToClearRoom == 0)
		{
			if (currentRoom <= (doors.Length - 1))
			{
				doors[currentRoom].Open();
			}
		}
	}

	public void ResetItems()
	{
		goldenKey.SetActive(true);
		
		for (int i = 0; i < doors.Length; i++)
		{
			doors[i].Reset();
		}
	}
}
