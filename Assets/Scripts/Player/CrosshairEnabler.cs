﻿using UnityEngine;

public class CrosshairEnabler : MonoBehaviour
{
	public GameObject crosshairGO;

	void Start()
	{
		crosshairGO.SetActive(GameManager.Instance.crosshairEnabled);
	}
}
