﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    private const float FIXED_Y_POS = 2f;
    private const float CAMERA_ROTATION_LIMIT = 85f;
    
    public Camera playerCamera;
	public PlayerGun playerGun;

    public float speed = 5f;

    public float lookSensitivityX = 3f;
    public float lookSensitivityY = 3f;
    public float smoothTime = 10f;

    public float maxHealth = 100f;

    public float deadAnimTime = 2f;

    private Vector3 velocity = Vector3.zero;
 
    private float cameraRotation = 0f;
    private float currentCameraRotation = 0f;
    
    private Rigidbody rb;

    private GameManager gameManager;
    private SoundManager soundManager;

    private float health = 0;
    public float Health {get {return health;}}

    private bool isDead = false;
    private bool doingDeadAnimation = false;

    private Vector3 initialDeadPosition = Vector3.zero;
    private Vector3 initialDeadRotation = Vector3.zero;
    private Vector3 finalDeadPosition = Vector3.zero;
    private Vector3 finalDeadRotation = Vector3.zero;
    private Timer deadTimer = null;

    private AudioSource audioSource;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();

        gameManager = GameManager.Instance;
        soundManager = SoundManager.Instance;

        deadTimer = new Timer(deadAnimTime);

        audioSource = GetComponent<AudioSource>();
    }

    private void Init()
    {
        health = maxHealth;
        
        Vector3 currentPos = transform.localPosition;
        currentPos.y = FIXED_Y_POS;
        transform.localPosition = currentPos;

        velocity = Vector3.zero;
        cameraRotation = 0f;
        currentCameraRotation = 0f;
    }

    public void Reset()
    {
        Init();
        
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        isDead = false;
        doingDeadAnimation = false;

        playerGun.gameObject.SetActive(true);
        playerGun.Reset();
    }

    void Update()
    {
        if (isDead)
        {
            if (doingDeadAnimation)
            {
                deadTimer.Update(Time.deltaTime);
                float progress = deadTimer.GetPercentage();

                transform.localPosition = Vector3.Lerp(initialDeadPosition, finalDeadPosition, progress);
                transform.localEulerAngles = Vector3.Lerp(initialDeadRotation, finalDeadRotation, progress);

                if (deadTimer.HasFinished())
                {
                    doingDeadAnimation = false;
                    gameManager.LoseGame();
                }
            }
            return;
        }    

        /*------------- MOVEMENT -------------*/
        //Calculate movement velocity as a 3D vector
        float xMov = Input.GetAxis("Horizontal");
		float zMov = Input.GetAxis("Vertical");

		Vector3 movHorizontal = transform.right * xMov;
		Vector3 movVertical = transform.forward * zMov;

		// Final movement vector
        velocity = (movHorizontal + movVertical) * speed;
        /*------------- /MOVEMENT -------------*/   

        /*------------- ROTATION X -------------*/
         //Calculate rotation as a 3D vector (turning around)
		float xRot = Input.GetAxisRaw("Mouse X") * lookSensitivityX;
        Quaternion newPlayerRotation = transform.localRotation * Quaternion.Euler(new Vector3(0f, xRot, 0f));
        transform.localRotation = Quaternion.Slerp(transform.localRotation, newPlayerRotation, smoothTime * Time.deltaTime);

        /*------------- /ROTATION X -------------*/

        /*------------- CAMERA ROTATION -------------*/
        //Calculate camera rotation (vertical)
		cameraRotation = Input.GetAxisRaw("Mouse Y") * lookSensitivityY;
	
        //Set our rotation and clamp it
        currentCameraRotation -= cameraRotation;
        currentCameraRotation = Mathf.Clamp(currentCameraRotation, -CAMERA_ROTATION_LIMIT, CAMERA_ROTATION_LIMIT);
        
        //Apply our rotation to the transform of our camera
        Quaternion cameraRot = Quaternion.Euler(new Vector3(currentCameraRotation, 0f, 0f));
        playerCamera.transform.localRotation = Quaternion.Slerp(playerCamera.transform.localRotation, cameraRot, smoothTime * Time.deltaTime);
	
        /*-------------  /CAMERA ROTATION -------------*/
    }

    void FixedUpdate()
	{
        if (isDead)
            return;

        if (velocity != Vector3.zero)
		{
			rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
		}   
    }

    public void TakeDamage(float damage)
    {
        if (isDead)
            return;
        
        health -= damage;
        isDead = health <= 0f;

        if (isDead && !gameManager.godMode)
        {
            playerGun.gameObject.SetActive(false);
            doingDeadAnimation = true;
            deadTimer.Reset();
            
            initialDeadPosition = transform.localPosition;
            initialDeadRotation = transform.localEulerAngles;
            finalDeadPosition = new Vector3(initialDeadPosition.x, 0.05f, initialDeadPosition.z);
            finalDeadRotation =  new Vector3(initialDeadRotation.x,initialDeadRotation.y, -15f);

            audioSource.clip = soundManager.GetAudioClip(Random.Range(0, 10) > 5 ? AudioNames.PLAYER_DEAD_1 : AudioNames.PLAYER_DEAD_2);
            audioSource.Play();
        }
        else
        {
            audioSource.clip = soundManager.GetAudioClip(AudioNames.PLAYER_INJURED);
            audioSource.Play();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        LayerMask colliderLayer = collision.gameObject.layer;

        if (colliderLayer == LevelManager.enemyLayer)
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }
    }
}