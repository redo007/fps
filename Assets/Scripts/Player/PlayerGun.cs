﻿using UnityEngine;

public class PlayerGun : MonoBehaviour
{
	public Camera playerCamera;
	public Animator animator;
	public ShotgunAnimEvents shotgunAnimEvents;

	public float damage = 50f;
	public float range = 35f;
	public float maxDistanceDobleDamage = 10f;

	private bool canShoot = true;

	private AudioSource audioSource;

	void Start()
	{
		audioSource = GetComponent<AudioSource>();
		audioSource.clip = SoundManager.Instance.GetAudioClip(AudioNames.SHOTGUN_FIRE);
		shotgunAnimEvents.OnReloadAnimEnd = ReloadAnimEnd;
	}

	void Update()
	{
		if (!canShoot)
			return;

		if (Input.GetButtonDown("Fire1"))
		{
			animator.SetTrigger("Fire");
			audioSource.Play();
			Shoot();
			canShoot = false;
		}
	}

	public void Reset()
	{
		animator.Rebind();
		canShoot = true;
	}

	private void Shoot()
	{
		RaycastHit hitInfo;
		
		if (Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out hitInfo, range))
		{
			EnemyIA enemy = hitInfo.transform.GetComponent<EnemyIA>();
			if (enemy != null)
			{
				bool closeRange = Vector3.Distance(playerCamera.transform.position, hitInfo.transform.position) <= maxDistanceDobleDamage;
				enemy.TakeDamage(closeRange ? damage * 2f : damage , closeRange);
			}
		}
	}

	public void ReloadAnimEnd()
	{
		canShoot = true;
	}
}