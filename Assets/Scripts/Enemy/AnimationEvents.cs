﻿using UnityEngine;

public class AnimationEvents : MonoBehaviour
{
	public System.Action OnAnimEventFire = null;
	public System.Action OnAnimEventEnd = null;

	public void AttackEventFire()
	{
		if (OnAnimEventFire != null)
			OnAnimEventFire();
	}

	public void AttackEventEnd()
	{
		if (OnAnimEventEnd != null)
			OnAnimEventEnd();
	}
}