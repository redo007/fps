﻿using UnityEngine;
using System.Collections.Generic;

public enum EnemyStates
{
    WALKING,
    SHOOTING,
    HIT,
    DEAD,
}

public enum WalkingAnimations
{
    FRONT       = 0,
    FRONT_SIDE  = 1,
    SIDE        = 2,
    BACK_SIDE   = 3,
    BACK        = 4
}

public class EnemyIA : MonoBehaviour
{
    public float speed = 5f;
    public float changeDirTime = 5f;
    public float fiexdYPos = 0f;
    public float maxShootTime = 10f;
    public float minShootTime = 0.5f;

    public float pauseOnHitTime = 0.1f;

    public Transform proyectileEmmiter;
    public Animator animator;
    public SpriteRenderer spriteRenderer;
    public Collider capsuleCollider;
    public AnimationEvents animationEvents;

    public Sprite[] hitSprites;

    private Rigidbody rb;   

    private LevelManager levelManager;
    private GameManager gameManager;
    private ProyectilesManager proyectilesManager;
    private SoundManager soundManager;

    private float life = 100f;
    private int wallLayer;
 
    private EnemyStates currentState = EnemyStates.WALKING;

    private Timer shootTimer = null;
    private Timer dirTimer = null;
    private Timer pauseOnHitTimer = null;
    private Timer walkSoundTimer = null;

    private WalkingAnimations currentWalkingAnimation;
    private Transform playerTransform;

    private AudioSource statesAudioSource;
    private AudioSource proyectileAudioSource;

    private void Start()
    {    
        levelManager = LevelManager.Instance;
        gameManager = GameManager.Instance;
        proyectilesManager = ProyectilesManager.Instance;
        soundManager = SoundManager.Instance;
        
        rb = GetComponent<Rigidbody>();

        statesAudioSource = GetComponent<AudioSource>();

        proyectileAudioSource = proyectileEmmiter.GetComponent<AudioSource>();
        proyectileAudioSource.clip = soundManager.GetAudioClip(AudioNames.IMP_ATTACK);

        Vector3 currentPos = transform.localPosition;
        currentPos.y = fiexdYPos;
        transform.localPosition = currentPos;

        animationEvents.OnAnimEventFire = AttackEventFire;
        animationEvents.OnAnimEventEnd = AttackEventEnd;
        
        shootTimer      = new Timer(Random.Range(minShootTime, maxShootTime));
        dirTimer        = new Timer(changeDirTime);
        pauseOnHitTimer = new Timer(pauseOnHitTime);
        walkSoundTimer  = new Timer(Random.Range(1f, 5f));

        playerTransform = levelManager.GetPlayerTransform();

        //Clean animator parameters
        animator.SetInteger("Die", 0);
    }

    private void Update()
    {
        animator.transform.LookAt(playerTransform);
      
        switch (currentState)
        {
            case EnemyStates.WALKING:
            {
                dirTimer.Update(Time.deltaTime);
                walkSoundTimer.Update(Time.deltaTime);

                if (dirTimer.HasFinished())
                    ChangeWalkingDirection();

                if (walkSoundTimer.HasFinished())
                {
                    walkSoundTimer.Reset(Random.Range(1f, 8f));
                    statesAudioSource.Play();
                }

                UpdateWalkinAnimAngle();
                UpdateShootTime();
            }
            break;

            case EnemyStates.SHOOTING:
            break;

            case EnemyStates.HIT:
            {
                pauseOnHitTimer.Update(Time.deltaTime);

                if (pauseOnHitTimer.HasFinished())
                {
                    animator.enabled = true;
                    ChangeState(EnemyStates.WALKING);
                }
            }
            break;
            case EnemyStates.DEAD: break;
        }
    }

    private void UpdateShootTime()
    {
        shootTimer.Update(Time.deltaTime);
    
        if (shootTimer.HasFinished())
        {
            ChangeState(EnemyStates.SHOOTING);
        }   
    }

    private void ChangeState(EnemyStates newState)
    {
        switch (newState)
        {
            case EnemyStates.WALKING: 
            {
                statesAudioSource.clip = soundManager.GetAudioClip(AudioNames.IMP_NEAR);
            }
            break;

            case EnemyStates.SHOOTING:
            {
                shootTimer.Reset(Random.Range(minShootTime, maxShootTime));
                animator.SetTrigger("Attack");
                proyectileAudioSource.Play();
            }
            break;

            case EnemyStates.HIT:
            {
                spriteRenderer.sprite = hitSprites[(int)currentWalkingAnimation];
                animator.enabled = false;

                statesAudioSource.clip = soundManager.GetAudioClip(AudioNames.IMP_INJURED);
                statesAudioSource.Play();
            }
            break;

            case EnemyStates.DEAD: break;
        }

        currentState = newState;
    }

    private void UpdateWalkinAnimAngle()
    {
        Vector3 dirToTarget = (playerTransform.position - transform.position).normalized;
        float angle = Vector3.Angle(transform.forward, dirToTarget);
        
        if (angle < 22.5f)
        {
            if (currentWalkingAnimation != WalkingAnimations.FRONT)
                ChangeWalkingAnimation(WalkingAnimations.FRONT);
        }
        else if (angle < 67.5f)
        {
            if (currentWalkingAnimation != WalkingAnimations.FRONT_SIDE)
                ChangeWalkingAnimation(WalkingAnimations.FRONT_SIDE);
        }
        else if (angle < 112.5f)
        {
            if (currentWalkingAnimation != WalkingAnimations.SIDE)
                ChangeWalkingAnimation(WalkingAnimations.SIDE);
        }
        else if (angle < 157.5f)
        {
            if (currentWalkingAnimation != WalkingAnimations.BACK_SIDE)
                ChangeWalkingAnimation(WalkingAnimations.BACK_SIDE);
        }
        else
        {
            if (currentWalkingAnimation != WalkingAnimations.BACK)
                ChangeWalkingAnimation(WalkingAnimations.BACK);
        }

        //Excludes FRONT and BACK anims
        if ((int)currentWalkingAnimation > 0 && (int)currentWalkingAnimation < 4)
        {
            float dotProd = Vector3.Dot(transform.right, dirToTarget);
            if (dotProd > 0)
               spriteRenderer.flipX = false;
            else if (dotProd < 0)
                spriteRenderer.flipX = true;
        }
    }

    private void ChangeWalkingAnimation(WalkingAnimations newWalkingAnimation)
    {
        currentWalkingAnimation = newWalkingAnimation;

        switch(currentWalkingAnimation)
        {
            case WalkingAnimations.FRONT:       animator.SetTrigger("WalkFront"); break;
            case WalkingAnimations.FRONT_SIDE:  animator.SetTrigger("WalkFrontSide"); break;
            case WalkingAnimations.SIDE:        animator.SetTrigger("WalkSide"); break;
            case WalkingAnimations.BACK_SIDE:   animator.SetTrigger("WalkBackSide"); break;
            case WalkingAnimations.BACK:        animator.SetTrigger("WalkBack"); break;
        }
    }

    private void ChangeWalkingDirection()
    {
        dirTimer.Reset();
        transform.Rotate(new Vector3(0f, 180f, 0f));
    }

    private void AttackEventFire()
    {
       proyectilesManager.ShootProyectile(proyectileEmmiter.position);
    }

    private void AttackEventEnd()
    {
        ChangeWalkingAnimation(currentWalkingAnimation);
        ChangeState(EnemyStates.WALKING);
    }
    
    private void FixedUpdate()
    {
        if (currentState == EnemyStates.WALKING)
        {
            rb.MovePosition(rb.position + (transform.forward * speed) * Time.fixedDeltaTime);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        LayerMask colliderLayer = collision.gameObject.layer;

        if (rb != null)
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }

        if (colliderLayer == LevelManager.wallLayer)
            ChangeWalkingDirection();
    }

    public void TakeDamage(float damage, bool closeRange)
    {
        life -= damage;

        if (life <= 0f)
        {
            currentState = EnemyStates.DEAD;
            capsuleCollider.enabled = false;

            if (closeRange)
            {
                animator.SetInteger("Die", 2);
                gameManager.Score += GameManager.INSTANT_KILL_SCORE;
                
                statesAudioSource.clip = soundManager.GetAudioClip(AudioNames.IMP_DEAD_3);
                statesAudioSource.Play();
            }
            else
            {
                animator.SetInteger("Die", 1);
                gameManager.Score += GameManager.KILL_SCORE;
                
                statesAudioSource.clip = soundManager.GetAudioClip(Random.Range(0, 10) > 5 ? AudioNames.IMP_DEAD_1 : AudioNames.IMP_DEAD_1);
                statesAudioSource.Play();
            }
            levelManager.OnEnemyKill();
        }
        else
        {
            ChangeState(EnemyStates.HIT);
        }
    }

}