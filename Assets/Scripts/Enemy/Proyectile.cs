﻿using UnityEngine;

public class Proyectile : MonoBehaviour
{
	public float speed = 10f;
	public float damage = 33f;
	public Rigidbody rb;

	private bool isActive = false;
	public bool IsActive {get {return isActive;}}

	public void Spawn(Vector3 position)
	{
		transform.position = position;
		transform.LookAt(LevelManager.Instance.GetPlayerTransform());
		
		isActive = true;
		gameObject.SetActive(isActive);
	}
	
	void FixedUpdate()
	{
		rb.MovePosition(rb.position + (transform.forward * speed) * Time.fixedDeltaTime);
	}

	private void OnTriggerEnter(Collider collider)
    {
		LayerMask colliderLayer = collider.gameObject.layer;
		if (colliderLayer != LevelManager.enemyLayer)
		{
			if (colliderLayer == LevelManager.playerLayer)
			{
				PlayerController player = collider.GetComponent<PlayerController>();
				player.TakeDamage(damage);			
			}	
			
			Reset();
		}
	}

	public void Reset()
	{
		isActive = false;
		gameObject.SetActive(isActive);

		rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
	}
}