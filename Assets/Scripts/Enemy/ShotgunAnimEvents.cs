﻿using UnityEngine;

public class ShotgunAnimEvents : MonoBehaviour
{
	public System.Action OnReloadAnimEnd = null;
	
	public void ReloadAnimEnd()
	{
		if (OnReloadAnimEnd != null)
			OnReloadAnimEnd();
	}
}