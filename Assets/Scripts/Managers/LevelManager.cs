﻿using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviourSingleton<LevelManager>
{
	public GameObject playerPrefab;
	public GameObject enemyPrefab;

	public static LayerMask wallLayer;
    public static LayerMask playerLayer;
    public static LayerMask enemyLayer;

	private PlayerController player = null;
	private Dictionary<int, List<GameObject>> enemiesDict = new Dictionary<int, List<GameObject>>();

	private Level currentLevel;

	private ProyectilesManager proyectilesManager; 

	protected override void Initialize()
	{
		wallLayer   = LayerMask.NameToLayer("Wall");
        playerLayer = LayerMask.NameToLayer("Player");
        enemyLayer  = LayerMask.NameToLayer("Enemy");

		proyectilesManager = ProyectilesManager.Instance;
	
		GameObject playerGO = Instantiate(playerPrefab);
		playerGO.SetActive(false);
		player = playerGO.GetComponent<PlayerController>();
	}

	public void StartLevel(Level level)
	{
		currentLevel = level;
	
		proyectilesManager.Reset();

		//Clean previous enemies
		for (int i = 0; i < enemiesDict.Count; i++)
		{
			List<GameObject> enemies = enemiesDict[i];
			if (enemies != null)
			{
				for (int j = 0; j < enemies.Count; j++)
					Destroy(enemies[j]);
			}
		}
		enemiesDict.Clear();

		player.gameObject.SetActive(true);
		player.transform.position = currentLevel.playerSpawnPoint.position;
		player.transform.rotation = currentLevel.playerSpawnPoint.rotation;
		player.Reset();

		//Creates the enemies
		for (int i = 0; i < currentLevel.enemySpawnPointsParent.childCount; i++)
		{
			Transform roomTransform = currentLevel.enemySpawnPointsParent.GetChild(i);
			enemiesDict[i] = new List<GameObject>();

			for (int j = 0; j < roomTransform.childCount; j++)
			{
				Transform spawnPoint = roomTransform.GetChild(j);

				if (spawnPoint.gameObject.activeInHierarchy == false)
					continue;

				GameObject enemyGO = Instantiate(enemyPrefab);
				enemyGO.SetActive(false);
				enemyGO.transform.SetParent(level.transform);
				enemyGO.transform.position = spawnPoint.position;
				enemyGO.transform.rotation = spawnPoint.rotation;

				enemiesDict[i].Add(enemyGO);
			}
		}

		EnablePlayersForRoom(0);
	}

	public void EnablePlayersForRoom(int room)
	{
		if (!enemiesDict.ContainsKey(room))
		{
			return;
		}

		int killsAmount = 0;

		for (int i = 0; i < enemiesDict[room].Count; i++)
		{
			enemiesDict[room][i].SetActive(true);
			killsAmount++;
		}

		currentLevel.SetKillsAmountAndRoom(killsAmount, room);
	}

	public void OnEnemyKill()
	{
		currentLevel.OnEnemyKill();
	}

	public void OnDoorOpen(int newRoomIndx)
	{
		EnablePlayersForRoom(newRoomIndx);
	}

	public Transform GetPlayerTransform()
    {
        return player.transform;
    }

	public PlayerController GetPlayer()
	{
		return player;
	}

	public void Stop()
	{
		proyectilesManager.Reset();
		player.gameObject.SetActive(false);
	}

	public void Restart()
	{
		currentLevel.ResetItems();
		StartLevel(currentLevel);
	}
}