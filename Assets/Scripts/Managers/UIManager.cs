﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ScreensId
{
	HUD_SCREEN,
	LOSE_SCREEN,
	MAIN_MENU_SCREEN,
	CREDITS_SCREEN,
	WIN_SCREEN
}

public class UIManager : MonoBehaviourSingleton<UIManager>
{
	private const string SCREENS_PATH = "UI/";
	
	private Dictionary<ScreensId, GameObject> screensDict = new Dictionary<ScreensId, GameObject>();

	private ScreensId currentScreenId;

	protected override void Initialize()
	{
		screensDict[ScreensId.HUD_SCREEN]		= Instantiate(Resources.Load<GameObject>(SCREENS_PATH + "HudScreen"));
		screensDict[ScreensId.LOSE_SCREEN]		= Instantiate(Resources.Load<GameObject>(SCREENS_PATH + "LoseScreen"));
		screensDict[ScreensId.MAIN_MENU_SCREEN]	= Instantiate(Resources.Load<GameObject>(SCREENS_PATH + "MainMenuScreen"));
		screensDict[ScreensId.CREDITS_SCREEN]	= Instantiate(Resources.Load<GameObject>(SCREENS_PATH + "CreditsScreen"));
		screensDict[ScreensId.WIN_SCREEN]	= Instantiate(Resources.Load<GameObject>(SCREENS_PATH + "WinScreen"));

		foreach(KeyValuePair<ScreensId, GameObject> screenPair in screensDict)
		{
			GameObject screenGO = screenPair.Value;

			screenGO.transform.SetParent(transform);

			RectTransform rectTransform = (RectTransform)screenGO.transform;
			rectTransform.offsetMax = new Vector2(0f, 0f);

			screenGO.SetActive(false);
		}

		ShowScreen(ScreensId.MAIN_MENU_SCREEN);
	}

	public void ShowScreen(ScreensId screenId)
	{
		screensDict[currentScreenId].SetActive(false);
		screensDict[screenId].SetActive(true);
		currentScreenId = screenId;
	}
}