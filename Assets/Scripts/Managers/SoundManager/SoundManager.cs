﻿using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviourSingleton<SoundManager>
{
	private Dictionary<string, AudioClip> clipsDict = new Dictionary<string, AudioClip>();

	public AudioSource musicAudioSource;
	public AudioSource soundsAudioSource;
	private string currentMusic = "";

	private AudioListener ownAudioListener;

	protected override void Initialize()
	{
		AudioClip[] soundsClips = Resources.LoadAll<AudioClip>("Audio/Sound");
		AudioClip[] musicClips = Resources.LoadAll<AudioClip>("Audio/Music");

		for (int i = 0; i < soundsClips.Length; i++)
		{
			clipsDict.Add(soundsClips[i].name, soundsClips[i]);
		}

		for (int i = 0; i < musicClips.Length; i++)
		{
			clipsDict.Add(musicClips[i].name, musicClips[i]);
		}

		ownAudioListener = GetComponent<AudioListener>();
	}

	public AudioClip GetAudioClip(string name)
	{
		if (clipsDict.ContainsKey(name))
		{
			return clipsDict[name];
		}

		Debug.LogError("No Audio Clip with name " + name);
		return null;
	}

	public void PlayMusic(string name)
	{
		if (musicAudioSource.isPlaying)
		{
			if (string.Compare(currentMusic, name) == 0)
				return;

			musicAudioSource.Stop();
		}

		currentMusic = name;
		musicAudioSource.clip = GetAudioClip(name);
		musicAudioSource.Play();
	}

	public void PlaySound(string name)
	{
		soundsAudioSource.clip = GetAudioClip(name);
		soundsAudioSource.Play();
	}

	public void StopMusic()
	{
		musicAudioSource.Stop();
	}

	public void ToogleAudioListener(bool value)
	{
		ownAudioListener.enabled = value;
	}
}