﻿
public class AudioNames
{
	public const string MENU_MUSIC			= "menu";
	public const string GAMEPLAY_MUSIC_0	= "gameplayMusic0";
	public const string GAMEPLAY_MUSIC_2	= "gameplayMusic1";
	public const string GAMEPLAY_MUSIC_1	= "gameplayMusic2";
	
	public const string SHOTGUN_FIRE	= "shotgun";
	public const string DOOR_OPEN		= "doorOpen";

	public const string PLAYER_INJURED	= "playerInjured";
	public const string PLAYER_DEAD_1	= "playerDead1";
	public const string PLAYER_DEAD_2	= "playerDead2";

	public const string IMP_INJURED	= "impInjured";
	public const string IMP_DEAD_1	= "impDeath1";
	public const string IMP_DEAD_2	= "impDeath2";
	public const string IMP_DEAD_3	= "impDeathSplat";
	public const string IMP_NEAR	= "impNearby";
	public const string IMP_ATTACK	= "impAttackFar";

	public const string MENU_MOVE	= "menuMove";
	public const string MENU_SELECT	= "menuSelect";
}