﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviourSingleton<GameManager>
{
    public const int KILL_SCORE = 10;
    public const int INSTANT_KILL_SCORE = 50;

    private const string LEVEL_NAME = "Level";

    public bool crosshairEnabled = false;
    public bool godMode = false;
    public float loseScreenTime = 3f;

    private LevelManager levelManager;
    private UIManager uIManager;
    private SoundManager soundManager;

    private Timer loseScreeTimer = null;
    private bool showLoseScreen = false;

    private int score = 0;
    public int Score {get {return score;} set {score = value;}}

    protected override void Initialize()
    {
        levelManager = LevelManager.Instance;
        uIManager = UIManager.Instance;
        soundManager = SoundManager.Instance;

        Cursor.visible = false;

        loseScreeTimer = new Timer(loseScreenTime);
    }

    public void StartGame()
    {
        Time.timeScale = 1f;
        uIManager.ShowScreen(ScreensId.HUD_SCREEN);
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.LoadScene(LEVEL_NAME, LoadSceneMode.Additive);

        soundManager.ToogleAudioListener(false);
        soundManager.PlayMusic(AudioNames.GAMEPLAY_MUSIC_0);
    }

    public void BackToMainMenu()
    {
        SceneManager.UnloadSceneAsync(LEVEL_NAME);
        uIManager.ShowScreen(ScreensId.MAIN_MENU_SCREEN);
    }

    void Update()
    {
        if (showLoseScreen)
        {
            loseScreeTimer.Update(Time.deltaTime);

            if (loseScreeTimer.HasFinished())
            {
                StopGame();
                uIManager.ShowScreen(ScreensId.LOSE_SCREEN);
                showLoseScreen = false;
            }
        }

        //Cheats
        if (Input.GetKeyDown(KeyCode.P))
        {
            RestartGame();
        }
        else if (Input.GetKeyDown(KeyCode.O))
        {
            StopGame();
            uIManager.ShowScreen(ScreensId.LOSE_SCREEN);
        }
        else if (Input.GetKeyDown(KeyCode.I))
        {
            WinGame();
        } 
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (string.Compare(scene.name, LEVEL_NAME) == 0)
        {
            Cursor.lockState = CursorLockMode.Locked;
            uIManager.ShowScreen(ScreensId.HUD_SCREEN);
            levelManager.StartLevel(GameObject.Find(LEVEL_NAME).GetComponent<Level>());
        }
    }

    private void StopGame()
    {
        Time.timeScale = 0f;
        levelManager.Stop();

        soundManager.ToogleAudioListener(true);
        soundManager.StopMusic();
    }

    public void LoseGame()
    {
        showLoseScreen = true;
        loseScreeTimer.Reset();
    }

    public void WinGame()
    {
        StopGame();
        uIManager.ShowScreen(ScreensId.WIN_SCREEN);
    }

    public void RestartGame()
    {
        score = 0;
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1f;
        uIManager.ShowScreen(ScreensId.HUD_SCREEN);
        levelManager.Restart();

        string music = "";

        switch (Random.Range(0, 3))
        {
            case 0: music = AudioNames.GAMEPLAY_MUSIC_0; break;
            case 1: music = AudioNames.GAMEPLAY_MUSIC_1; break;
            case 2: music = AudioNames.GAMEPLAY_MUSIC_2; break;
        }

        soundManager.ToogleAudioListener(false);
        soundManager.PlayMusic(music);
    }
}