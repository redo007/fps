﻿using System.Collections.Generic;
using UnityEngine;

public class ProyectilesManager : MonoBehaviourSingleton<ProyectilesManager>
{
	public int initialAmount = 10;

	public GameObject projectilePrefab;
	
	private List<Proyectile> proyectiles = new List<Proyectile>();
	
	protected override void Initialize()
	{
		for (int i = 0; i < initialAmount; i++)
		{
			CreateProyectile();
		}
	}

	private Proyectile CreateProyectile()
	{
		GameObject proyectileGO = Instantiate(projectilePrefab, Vector3.zero, Quaternion.identity, transform);
		proyectileGO.SetActive(false);

		Proyectile proyectile = proyectileGO.GetComponent<Proyectile>();
		proyectiles.Add(proyectile);

		return proyectile;
	}

	public void ShootProyectile(Vector3 initialPosition)
	{
		for (int i = 0; i < proyectiles.Count; i++)
		{
			if (!proyectiles[i].IsActive)
			{
				proyectiles[i].Spawn(initialPosition);
				return;
			}
		}

		CreateProyectile().Spawn(initialPosition);
	}

	public void Reset()
	{
		for (int i = 0; i < proyectiles.Count; i++)
		{
			proyectiles[i].Reset();
		}
	}
}