
public class Singleton <T> where T : Singleton<T>, new()
{
	static T instance;

	public static T Instance
	{
		get 
		{
            if (instance == null)
			{
				instance = new T();
				instance.Initialize();
			}

			return instance;
		}
	}

	protected virtual void Initialize()
	{
	}
}