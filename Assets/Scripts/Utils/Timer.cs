﻿
public class Timer
{
	private float duration		= 0f;
	private float currentTime	= 0f;
	
	private bool finished = false;

	public Timer(float duration)
	{
		Reset(duration);
	}

	public void Reset(float newDuration = 0f)
	{
		if (newDuration != 0f)
			duration = newDuration;
	
		currentTime = 0f;
		finished = false;
	}

	public void Update(float dt)
	{
		if (finished)
			return;

		currentTime += dt;
		finished = currentTime >= duration;
	}

	public bool HasFinished()
	{
		return finished;
	}

	public float GetPercentage()
	{
		return currentTime / duration;
	}
}