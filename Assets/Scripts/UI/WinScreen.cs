﻿using UnityEngine;
using UnityEngine.UI;

public class WinScreen : OptionScreen
{
	public Text scoreText;

	protected override void Start()
	{
		base.Start();
		scoreText.text = string.Format("{0}", GameManager.Instance.Score);

		SoundManager.Instance.PlayMusic(AudioNames.MENU_MUSIC);
	}

	protected override void InitOptions()
	{
		optionsList[0].ChooseCalback = OnRestartSelected;
		optionsList[1].ChooseCalback = OnMainMenuSelected;
	}

	private void OnMainMenuSelected()
	{
		GameManager.Instance.BackToMainMenu();
	}
	
	private void OnRestartSelected()
	{
		GameManager.Instance.RestartGame();
	}
}