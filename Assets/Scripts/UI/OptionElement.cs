﻿using UnityEngine;

public class OptionElement : MonoBehaviour
{
	public System.Action ChooseCalback = null;

	public void Select()
	{
		if (ChooseCalback != null)
			ChooseCalback();
	}
}