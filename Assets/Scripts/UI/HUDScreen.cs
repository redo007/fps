﻿using UnityEngine;
using UnityEngine.UI;

public class HUDScreen : MonoBehaviour
{
	public Text healthValue;
	public Text scoreValue;

	private GameManager gameManager;
	private PlayerController player;
	
	private float prevHealth = 0f;
	private int prevScore = -1;

	void Start()
	{
		player = LevelManager.Instance.GetPlayer();
		gameManager = GameManager.Instance;
	}

	void Update()
	{
		if (prevHealth != player.Health)
		{
			healthValue.text = string.Format("{0}%", Mathf.Max(0f, player.Health));
		}

		if (prevScore != gameManager.Score)
		{
			scoreValue.text = string.Format("{0}", gameManager.Score);
		}
	}
}