﻿using UnityEngine;

public class LoseScreen : OptionScreen
{
	protected override void InitOptions()
	{
		optionsList[0].ChooseCalback = OnRestartSelected;
		optionsList[1].ChooseCalback = OnMainMenuSelected;

		SoundManager.Instance.PlayMusic(AudioNames.MENU_MUSIC);
	}

	private void OnMainMenuSelected()
	{
		GameManager.Instance.BackToMainMenu();
	}
	
	private void OnRestartSelected()
	{
		GameManager.Instance.RestartGame();
	}
}