﻿using UnityEngine;

public class CreditsScreen : MonoBehaviour
{
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Escape))
		{
			UIManager.Instance.ShowScreen(ScreensId.MAIN_MENU_SCREEN);
		}	
	}
}
