﻿using UnityEngine;

public class MainMenuScreen : OptionScreen
{
	protected override void InitOptions()
	{
		optionsList[0].ChooseCalback = OnPlaySelected;
		optionsList[1].ChooseCalback = OnCreditsSelected;
		optionsList[2].ChooseCalback = OnQuitSelected;

		SoundManager.Instance.PlayMusic(AudioNames.MENU_MUSIC);
	}

	private void OnPlaySelected()
	{
		GameManager.Instance.StartGame();
	}
	
	private void OnCreditsSelected()
	{
		UIManager.Instance.ShowScreen(ScreensId.CREDITS_SCREEN);
	}

	private void OnQuitSelected()
	{
		Application.Quit();
	}
}