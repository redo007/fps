﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionScreen : MonoBehaviour
{
	public Transform selector;
	public OptionElement[] optionsList;

	private int currentOptionIndex = 0;
	private int maxIndex = 0;

	private SoundManager soundManager;

	protected virtual void Start() 
	{
		maxIndex = optionsList.Length - 1;

		Canvas.ForceUpdateCanvases();
		MoveSelector();

		soundManager = SoundManager.Instance;

		InitOptions();
	}

	protected virtual void InitOptions()
	{
	}
	
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			ChangeOption(1);
		}
		else if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			ChangeOption(-1);
		}
		else if (Input.GetKeyDown(KeyCode.Return))
		{
			SelectOption();
		}
	}

	private void ChangeOption(int dir)
	{
		currentOptionIndex += dir;

		if (currentOptionIndex < 0)
			currentOptionIndex = maxIndex;
		else if (currentOptionIndex > maxIndex)
			currentOptionIndex = 0;

		MoveSelector();
		soundManager.PlaySound(AudioNames.MENU_MOVE);
	}

	private void MoveSelector()
	{
		Vector3 pos = selector.position;
		pos.y = optionsList[currentOptionIndex].transform.position.y;
		selector.position = pos;
	}

	private void SelectOption()
	{
		soundManager.PlaySound(AudioNames.MENU_SELECT);
		optionsList[currentOptionIndex].Select();
	}
}
